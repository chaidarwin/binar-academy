package com.example.binaracademy

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.binaracademy.model.CategoryData
import kotlinx.android.synthetic.main.category_list.*
import kotlinx.android.synthetic.main.category_list_item.view.*

class CategoryList : AppCompatActivity() {

    private lateinit var listCategory : List<CategoryData>

    var adapter: CategoryAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.category_list)

        setToolbar()
        insertCategoryData()
        setRecycleV()
    }

    private fun setToolbar(){
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.category_list)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setHomeButtonEnabled(false)
    }

    private fun insertCategoryData(){
        listCategory = listOf(
            CategoryData(name = "Business", value = "business"), CategoryData(name = "Entertainment", value = "entertainment"), CategoryData(name = "General", value = "general"),
            CategoryData(name = "Health", value = "health"), CategoryData(name = "Science", value = "science"), CategoryData(name = "Sports", value = "sports"),CategoryData(name = "Technology", value = "technology"),
        )
    }

    private fun setRecycleV() {
        category_list.layoutManager = LinearLayoutManager(this)
        category_list.isNestedScrollingEnabled = false
        adapter = CategoryAdapter(listCategory)
        category_list.adapter = adapter
    }

    inner class CategoryAdapter(private val category: List<CategoryData>) : RecyclerView.Adapter<CategoryHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): CategoryHolder {
            return CategoryHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.category_list_item, viewGroup, false))
        }

        override fun getItemCount(): Int = category.size

        override fun onBindViewHolder(holder: CategoryHolder, position: Int) {
            holder.bindCountry(category[position])

            holder.itemView.setOnClickListener {
                val i = Intent(holder.itemView.context,SourcesList::class.java)
                i.putExtra("category",category[position].value)
                holder.itemView.context.startActivity(i)
            }
        }
    }

    inner class CategoryHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val categoryNameText = view.category_name

        fun bindCountry(category: CategoryData) {
            categoryNameText.text = category.name
        }
    }


}