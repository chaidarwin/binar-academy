package com.example.binaracademy

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.news_detail.*
import android.webkit.WebViewClient
import android.annotation.SuppressLint
import android.view.KeyEvent
import com.example.binaracademy.io.EmptyTypeEn
import com.example.binaracademy.utilities.Utility
import kotlinx.android.synthetic.main.empty_layout.*
import kotlinx.android.synthetic.main.news_detail.empty_layout
import kotlinx.android.synthetic.main.news_detail.toolbar
import kotlinx.android.synthetic.main.news_list.*


class NewsDetail : AppCompatActivity() {

    var newsTitle:String?=null
    var source:String?=null
    var url:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.news_detail)

        getVar()
        setToolbar()
        setData()
    }

    private fun getVar(){
        newsTitle = intent.getStringExtra("title")
        source = intent.getStringExtra("source")
        url = intent.getStringExtra("url")
    }

    private fun setToolbar(){
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = newsTitle
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        (toolbar)?.setNavigationOnClickListener { onBackPressed() }
    }

    private fun setData(){
        if (Utility.checkInternetOn(this)) {
            if (url!=null){
                setWebView(url!!)
            }else{
                handleTimeoutOrNoInternet(EmptyTypeEn.Nodata.result)
            }
        }
        else{
            handleTimeoutOrNoInternet(EmptyTypeEn.Noconnection.result)
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setWebView(url:String){
        webView.settings.loadsImagesAutomatically = true
        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true

        webView.settings.setSupportZoom(true)
        webView.settings.builtInZoomControls = true
        webView.settings.displayZoomControls = false

        webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        webView.webViewClient = WebViewClient()

        webView.loadUrl(url)
        visibilityView(View.VISIBLE,View.GONE)
    }

    private fun handleTimeoutOrNoInternet(typeEn : String){
        showEmptyLayout(reloadClick,typeEn)
        visibilityView(View.GONE,View.VISIBLE)
    }

    private val reloadClick = View.OnClickListener {
        reload()
    }

    private fun reload() {
        visibilityView(View.GONE,View.GONE)
        setWebView(url!!)
    }

    private fun showEmptyLayout(click: View.OnClickListener,action: String){
        Utility.test(img,desc1,desc2,click_layout,click_text,click,action,this)
    }

    private fun visibilityView(showWeb:Int,empty: Int) {
        webView.visibility=showWeb
        empty_layout.visibility=empty
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && this.webView.canGoBack()) {
            this.webView.goBack()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
}