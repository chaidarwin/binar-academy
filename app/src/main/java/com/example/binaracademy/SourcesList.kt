package com.example.binaracademy

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.MenuItemCompat
import androidx.core.view.contains
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.binaracademy.ch.OnLoadMoreListener
import com.example.binaracademy.io.EmptyTypeEn
import com.example.binaracademy.model.SourceData
import com.example.binaracademy.utilities.Utility
import com.example.binaracademy.viewmodel.SourceListViewModel
import kotlinx.android.synthetic.main.empty_layout.*
import kotlinx.android.synthetic.main.source_list.*
import kotlinx.android.synthetic.main.source_list_item.view.*
import retrofit2.HttpException
import java.net.SocketTimeoutException

class SourcesList : AppCompatActivity() {

    private lateinit var sourceListViewModel: SourceListViewModel
    var data: MutableList<SourceData> = ArrayList()

    private var adapter: SourceAdapter?=null

    var page = 1
    var total : Int ?= null

    var category:String ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.source_list)

        getVar()
        setToolbar()
        initObject()
        getSourceList(category!!,"5",page.toString())
        initObserver()
        setRecycleV()
    }

    private fun getVar(){
        category = intent.getStringExtra("category")
    }

    private fun setToolbar(){
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.source_list)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setHomeButtonEnabled(false)
    }

    private fun initObject(){
        sourceListViewModel = ViewModelProviders.of(this).get(SourceListViewModel::class.java)
    }

    fun getSourceList(category: String, pageSize:String,page : String) {
        if (Utility.checkInternetOn(this)) {
            sourceListViewModel.getAllSourceData("","http://newsapi.org/v2/",category,"4fbf08a0679a442b8dd0b24c7ab1eac4",pageSize,page)
        } else {
            handleTimeoutOrNoInternet(EmptyTypeEn.Noconnection.result)
        }
    }

    private fun initObserver(){
        handleHttpError(sourceListViewModel.errorRequest)

        sourceListViewModel.errorRequest.observe(this, { callback->
            if (callback is SocketTimeoutException){
                handleTimeoutOrNoInternet(EmptyTypeEn.Timeout.result)
            } else{
                callback?.printStackTrace()
                resetList(EmptyTypeEn.Error.result)
            }
        })

        sourceListViewModel.observableSourceListData.observe(this, { callback ->
            if (callback?.status == "error") {
                resetList(EmptyTypeEn.Nodata.result)
            } else {
                total = callback!!.data.size
                if (callback.data.size == 0) {
                    resetList(EmptyTypeEn.Nodata.result)
                } else {
                    if (page > 1) {
                        sourceListViewModel.removeLoadingList(data)
                        adapter!!.notifyItemRemoved(data.size)
                    }

                    data.addAll(callback.data)

                    list.post {
                        adapter!!.notifyItemInserted(data.size)
                    }
                    adapter!!.setLoading()
                    visibilityView(View.VISIBLE, View.GONE)
                    swipeView.isRefreshing = false
                }
            }
        })
    }

    private fun <T: Throwable> handleHttpError(observable : MutableLiveData<T>) {
        observable.observe(this, {
            if (it is HttpException) {
                httpExceptionHandling(it)
            }
        })
    }

    private fun httpExceptionHandling(e: HttpException) {
        when (e.code()) {
            400 -> Toast.makeText(this,getString(R.string.bad_request), Toast.LENGTH_SHORT).show()
            410 -> Toast.makeText(this,getString(R.string.error_page_doesnt_exist), Toast.LENGTH_SHORT).show()
            401 -> Toast.makeText(this,getString(R.string.unauthorized), Toast.LENGTH_SHORT).show()
            403 -> Toast.makeText(this,getString(R.string.error_access_right), Toast.LENGTH_SHORT).show()
            404 -> Toast.makeText(this,getString(R.string.error_page_doesnt_exist), Toast.LENGTH_SHORT).show()
            408 -> Toast.makeText(this,getString(R.string.error_rto), Toast.LENGTH_SHORT).show()
            500 -> Toast.makeText(this,getString(R.string.error_internal_server), Toast.LENGTH_SHORT).show()
            else -> {
                Toast.makeText(this,getString(R.string.error_try_again), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun setRecycleV(){
        list.layoutManager = LinearLayoutManager(this)
        list.isNestedScrollingEnabled = false
        adapter = SourceAdapter(data)
        list.adapter = adapter

        adapter!!.setLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                if (data.size < total!! - 1) {
                    handleLoadingList()
                    page++
                    getSourceList(category!!,"5",page.toString())
                }
            }
        })

        swipeView.setColorSchemeResources(R.color.holo_blue_dark, R.color.holo_blue_light, R.color.holo_green_light, R.color.holo_green_dark)
        swipeView.setOnRefreshListener{
            data.clear()
            adapter?.notifyDataSetChanged()
            page = 1
            swipeView.isRefreshing = true
            swipeView.post {
                getSourceList(category!!,"5",page.toString())
            }
        }
    }

    private fun resetList(typeEn : String){
        data.clear()
        page = 1
        swipeView.isRefreshing = false
        showEmptyLayout(reloadClick, typeEn)
        visibilityView(View.GONE,View.VISIBLE)
    }

    private fun handleTimeoutOrNoInternet(typeEn : String){
        if (data.size!=0){
            sourceListViewModel.setRetryList(data)
            adapter!!.notifyDataSetChanged()
        }else{
            swipeView.isRefreshing = false
            showEmptyLayout(reloadClick,typeEn)
            visibilityView(View.GONE,View.VISIBLE)
        }
    }

    private val reloadClick = View.OnClickListener {
        reload()
    }

    private fun reload() {
        data.clear()
        visibilityView(View.GONE,View.GONE)
        swipeView.isRefreshing = false
        page = 1
        getSourceList(category!!,"5",page.toString())
    }

    private fun showEmptyLayout(click: View.OnClickListener,action: String){
        Utility.test(img,desc1,desc2,click_layout,click_text,click,action,this)
    }

    private fun visibilityView(list:Int,empty:Int){
        list_layout.visibility=list
        empty_layout.visibility=empty
    }

    fun handleLoadingList(){
        sourceListViewModel.addLoadingList(data)
        adapter!!.notifyDataSetChanged()
    }

    inner class SourceAdapter(private val data: MutableList<SourceData>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        private var holder: RecyclerView.ViewHolder? = null
        private var loadMoreListener : OnLoadMoreListener?=null
        private val viewRetry=2
        private val viewItem=1
        private val viewLoading=0
        var linearLayoutManager = list.layoutManager as LinearLayoutManager
        var totalItemCount : Int=0
        var lastItemVisible : Int=0
        var visibleItem : Int = 5
        var loading : Boolean = false

        init {
            list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    totalItemCount = linearLayoutManager.itemCount
                    lastItemVisible = linearLayoutManager.findLastVisibleItemPosition()

                    if (!loading && dy>0 && totalItemCount < lastItemVisible + visibleItem) {
                        if (loadMoreListener != null) {
                            loadMoreListener!!.onLoadMore()
                        }
                        loading = true
                    }
                }
            })
        }

        inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var sourceName : TextView = view.findViewById<View>(R.id.source_name) as TextView
            var description : TextView = view.findViewById<View>(R.id.description) as TextView
        }

        private inner class LoadingLayout(v: View) : RecyclerView.ViewHolder(v)
        private inner class RetryLayout(v: View) : RecyclerView.ViewHolder(v)

        override fun getItemViewType(position: Int): Int {
            return when {
                data[position].retry -> viewRetry
                data[position].name==null -> viewLoading
                else -> viewItem
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return when (viewType) {
                viewItem -> {
                    val v = LayoutInflater.from(this@SourcesList)
                        .inflate(R.layout.source_list_item, parent, false)
                    MyViewHolder(v)
                }
                viewLoading -> {
                    val v = LayoutInflater.from(this@SourcesList)
                        .inflate(R.layout.progress_item, parent, false)
                    LoadingLayout(v)
                }
                else -> {
                    val v = LayoutInflater.from(this@SourcesList)
                        .inflate(R.layout.retry_layout, parent, false)
                    RetryLayout(v)
                }
            }
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            this.holder = holder
            if (holder is MyViewHolder){
                holder.itemView.setOnClickListener {
                    val i = Intent(holder.itemView.context,NewsList::class.java)
                    i.putExtra("category",category)
                    i.putExtra("source",data[position].id)
                    i.putExtra("source_name",data[position].name)
                    i.putExtra("country",data[position].country)
                    holder.itemView.context.startActivity(i)
                }

                holder.sourceName.text= data[position].name?:""
                holder.description.text = data[position].description?:""

            }
            else if(holder is RetryLayout){
                holder.itemView.setOnClickListener {
                    data.removeAt(data.size-1)
                    handleLoadingList()
                    getSourceList(category!!,"5",page.toString())
                }
            }
        }

        override fun getItemCount(): Int {
            return data.size
        }

        fun setLoadMoreListener(onLoadMoreListener: OnLoadMoreListener){
            loadMoreListener = onLoadMoreListener
        }

        fun setLoading() {
            loading = false
        }
    }



}