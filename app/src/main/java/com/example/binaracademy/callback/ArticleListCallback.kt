package com.example.binaracademy.callback

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class ArticleListCallback<T> {

    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("articles")
    @Expose
    var data: MutableList<T> = ArrayList()
    @SerializedName("totalResults")
    @Expose
    var total: Int? = null
}