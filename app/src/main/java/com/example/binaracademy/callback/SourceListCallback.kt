package com.example.binaracademy.callback

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class SourceListCallback<T> {

    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("sources")
    @Expose
    var data: MutableList<T> = ArrayList()
    @SerializedName("error")
    @Expose
    var error: List<String>? = ArrayList()
}