package com.example.binaracademy.io

enum class EmptyTypeEn(val result: String) {
    Noconnection("NO_CONNECTION"),
    Error("ERROR"),
    Nodata("NO_DATA"),
    Nopageavailable("NO_PAGE_AVAILABLE"),
    Timeout("SocketTimeoutException"),
}