package com.example.binaracademy.io

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RestClientNews {

    private var gitApiInterface: ApiInterfaceNews.GitApiInterface? = null

    fun getClient(apiToken: String,baseUrl : String): ApiInterfaceNews.GitApiInterface {

        val logging = HttpLoggingInterceptor()
        logging.level= HttpLoggingInterceptor.Level.BODY
        val okClient = OkHttpClient.Builder()
            .addInterceptor(logging)
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                val original = chain.request()
                val request: Request
                if(apiToken==""){
                    request = original.newBuilder()
                        .header("Accept", "application/json")
                        .method(original.method(), original.body())
                        .build()
                }else{
                    request = original.newBuilder()
                        .header("Accept", "application/json")
                        .header("Authorization", apiToken)
                        .method(original.method(), original.body())
                        .build()
                    Log.d("token :",apiToken)
                }

                val response = chain.proceed(request)
                response
            }.build()

        val requestInterface = Retrofit.Builder()
            .baseUrl(if(baseUrl=="")("http://newsapi.org/v2/")else(baseUrl))
            .client(okClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        gitApiInterface =  requestInterface.create(ApiInterfaceNews.GitApiInterface::class.java)

        return gitApiInterface as ApiInterfaceNews.GitApiInterface
    }
}