package com.example.binaracademy.io

import com.example.binaracademy.callback.ArticleListCallback
import com.example.binaracademy.callback.SourceListCallback
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class BaseDataRx<T> {

    fun requestArticleList(observable: Observable<ArticleListCallback<T>>, disposableObserver: DisposableObserver<ArticleListCallback<T>>)
            : CompositeDisposable {

        return CompositeDisposable().apply { add(
            observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(disposableObserver)
        )
        }
    }

    fun requestSourceList(observable: Observable<SourceListCallback<T>>, disposableObserver: DisposableObserver<SourceListCallback<T>>)
            : CompositeDisposable {

        return CompositeDisposable().apply { add(
            observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(disposableObserver)
        )
        }
    }
}