package com.example.binaracademy.io

import com.example.binaracademy.callback.ArticleListCallback
import com.example.binaracademy.callback.SourceListCallback
import com.example.binaracademy.model.ArticleData
import com.example.binaracademy.model.SourceData
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

object ApiInterfaceNews {

    interface GitApiInterface{

        @GET("top-headlines/sources")
        fun getSourcesData(@Query("category") category:String?, @Query("apiKey") apiKey:String?,@Query("pageSize") pageSize:String?,@Query("page") page:String?): Observable<SourceListCallback<SourceData>>

        @GET("top-headlines")
        fun getHeadlineNewsData(@Query("sources") sources:String?, @Query("apiKey") apiKey:String?,@Query("pageSize") pageSize:String?,@Query("page") page:String?): Observable<ArticleListCallback<ArticleData>>

        @GET("everything")
        fun getAllNewsData( @Query("category") category:String?,@Query("sources") source:String?,@Query("country") country:String?,@Query("apiKey") apiKey:String?,@Query("pageSize") pageSize:String?,@Query("page") page:String?): Observable<ArticleListCallback<ArticleData>>

        @GET("everything")
        fun getQueryNewsData( @Query("q") query:String?,@Query("apiKey") apiKey:String?,@Query("pageSize") pageSize:String?,@Query("page") page:String?): Observable<ArticleListCallback<ArticleData>>
    }
}