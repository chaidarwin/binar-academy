package com.example.binaracademy.model

data class CategoryData (
    val name: String,
    val value: String
)

