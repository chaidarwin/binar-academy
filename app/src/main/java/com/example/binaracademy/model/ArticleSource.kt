package com.example.binaracademy.model

import com.example.binaracademy.ch.BaseInterface
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ArticleSource : BaseInterface{

    @SerializedName("id")
    @Expose
    val id: String? = null

    @SerializedName("name")
    @Expose
    val name: String? = null
}