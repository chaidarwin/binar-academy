package com.example.binaracademy

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.binaracademy.ch.OnLoadMoreListener
import com.example.binaracademy.io.EmptyTypeEn
import com.example.binaracademy.model.ArticleData
import com.example.binaracademy.utilities.Utility
import com.example.binaracademy.viewmodel.ArticleListViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.empty_layout.*
import kotlinx.android.synthetic.main.news_list.*
import retrofit2.HttpException
import java.net.SocketTimeoutException

class NewsList : AppCompatActivity() {

    private lateinit var articleListViewModel: ArticleListViewModel
    var data: MutableList<ArticleData> = ArrayList()

    private var adapter: ArticleAdapter?=null

    var page = 1
    var total : Int ?= null

    private var category:String ?= null
    private var source:String ?= null
    private var sourceName:String ?= null
    private var country:String ?= null

    var searchView : SearchView ? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.news_list)

        getVar()
        setToolbar()
        initObject()
        getNewsList(source!!,"5",page.toString())
        initObserver()
        setRecycleV()
    }

    private fun getVar(){
        category = intent.getStringExtra("category")
        source = intent.getStringExtra("source")
        sourceName = intent.getStringExtra("source_name")
        country = intent.getStringExtra("country")
    }

    private fun setToolbar(){
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = sourceName
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        (toolbar)?.setNavigationOnClickListener { onBackPressed() }
    }

    private fun initObject(){
        articleListViewModel = ViewModelProviders.of(this).get(ArticleListViewModel::class.java)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)

        val searchItem: MenuItem? = menu?.findItem(R.id.search)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView= searchItem?.actionView as SearchView
        searchView!!.maxWidth = Integer.MAX_VALUE

        searchView!!.setOnCloseListener {
            true
        }

        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                clearData(query!!)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })

        searchView!!.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        return super.onCreateOptionsMenu(menu)
    }

    private fun clearData(query: String){
        data.clear()
        adapter!!.setLoading()
        swipeView.isRefreshing = true
        getQueryNewsList(query,"5",page.toString())
        searchView!!.clearFocus()
    }

    override fun onBackPressed() {
        if (!searchView!!.isIconified) {
            searchView!!.onActionViewCollapsed()
        } else {
            super.onBackPressed();
        }
    }

    fun getNewsList(source : String,pageSize:String,page : String) {
        if (Utility.checkInternetOn(this)) {
            articleListViewModel.getHeadlineNewsData("","http://newsapi.org/v2/",source,"4fbf08a0679a442b8dd0b24c7ab1eac4",pageSize,page)
        } else {
            handleTimeoutOrNoInternet(EmptyTypeEn.Noconnection.result)
        }
    }

    fun getQueryNewsList(query : String,pageSize:String,page : String) {
        if (Utility.checkInternetOn(this)) {
            articleListViewModel.getQueryNewsData("","http://newsapi.org/v2/",query,"4fbf08a0679a442b8dd0b24c7ab1eac4",pageSize,page)
        } else {
            handleTimeoutOrNoInternet(EmptyTypeEn.Noconnection.result)
        }
    }

    private fun initObserver(){
        handleHttpError(articleListViewModel.errorRequest)

        articleListViewModel.errorRequest.observe(this, { callback->
            if (callback is SocketTimeoutException){
                handleTimeoutOrNoInternet(EmptyTypeEn.Timeout.result)
            } else{
                callback?.printStackTrace()
                resetList(EmptyTypeEn.Error.result)
            }
        })

        articleListViewModel.observableArticleListData.observe(this, { callback ->
            if (callback?.status == "error") {
                resetList(EmptyTypeEn.Nodata.result)
            } else {
                total = callback!!.total
                if (callback.data.size == 0) {
                    resetList(EmptyTypeEn.Nodata.result)
                } else {
                    if (page > 1) {
                        articleListViewModel.removeLoadingList(data)
                        adapter!!.notifyItemRemoved(data.size)
                    }

                    data.addAll(callback.data)

                    list.post {
                        adapter!!.notifyItemInserted(data.size)
                    }
                    adapter!!.setLoading()
                    visibilityView(View.VISIBLE, View.GONE)
                    swipeView.isRefreshing = false
                }
            }
        })
    }

    private fun <T: Throwable> handleHttpError(observable : MutableLiveData<T>) {
        observable.observe(this, {
            if (it is HttpException){
                httpExceptionHandling(it)
            }
        })
    }

    private fun httpExceptionHandling(e: HttpException) {
        when (e.code()) {
            400 -> Toast.makeText(this,getString(R.string.bad_request), Toast.LENGTH_SHORT).show()
            410 -> Toast.makeText(this,getString(R.string.error_page_doesnt_exist), Toast.LENGTH_SHORT).show()
            401 -> Toast.makeText(this,getString(R.string.unauthorized), Toast.LENGTH_SHORT).show()
            403 -> Toast.makeText(this,getString(R.string.error_access_right), Toast.LENGTH_SHORT).show()
            404 -> Toast.makeText(this,getString(R.string.error_page_doesnt_exist), Toast.LENGTH_SHORT).show()
            408 -> Toast.makeText(this,getString(R.string.error_rto), Toast.LENGTH_SHORT).show()
            500 -> Toast.makeText(this,getString(R.string.error_internal_server), Toast.LENGTH_SHORT).show()
            else -> {
                Toast.makeText(this,getString(R.string.error_try_again), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun setRecycleV(){
        list.layoutManager = LinearLayoutManager(this)
        list.isNestedScrollingEnabled = false
        adapter = ArticleAdapter(data)
        list.adapter = adapter

        adapter!!.setLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                if (data.size < total!! - 1) {
                    handleLoadingList()
                    page++
                    getNewsList(source!!,"5",page.toString())
                }
            }
        })

        swipeView.setColorSchemeResources(R.color.holo_blue_dark, R.color.holo_blue_light, R.color.holo_green_light, R.color.holo_green_dark)
        swipeView.setOnRefreshListener{
            data.clear()
            adapter?.notifyDataSetChanged()
            page = 1
            swipeView.isRefreshing = true
            swipeView.post {
                getNewsList(source!!,"5",page.toString())
            }
        }
    }

    private fun resetList(typeEn : String){
        data.clear()
        page = 1
        swipeView.isRefreshing = false
        showEmptyLayout(reloadClick, typeEn)
        visibilityView(View.GONE,View.VISIBLE)
    }

    private fun handleTimeoutOrNoInternet(typeEn : String){
        if (data.size!=0){
            articleListViewModel.setRetryList(data)
            adapter!!.notifyDataSetChanged()
        }else{
            swipeView.isRefreshing = false
            showEmptyLayout(reloadClick,typeEn)
            visibilityView(View.GONE,View.VISIBLE)
        }
    }

    private val reloadClick = View.OnClickListener {
        reload()
    }

    private fun reload() {
        data.clear()
        visibilityView(View.GONE,View.GONE)
        swipeView.isRefreshing = false
        page = 1
        getNewsList(source!!,"5",page.toString())
    }

    private fun showEmptyLayout(click: View.OnClickListener,action: String){
        Utility.test(img,desc1,desc2,click_layout,click_text,click,action,this)
    }

    private fun visibilityView(list:Int,empty:Int){
        list_layout.visibility=list
        empty_layout.visibility=empty
    }

    @SuppressLint("NotifyDataSetChanged")
    fun handleLoadingList(){
        articleListViewModel.addLoadingList(data)
        adapter!!.notifyDataSetChanged()
    }

    inner class ArticleAdapter(private val data: MutableList<ArticleData>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        private var holder: RecyclerView.ViewHolder? = null
        private var loadMoreListener : OnLoadMoreListener?=null
        private val viewRetry=2
        private val viewItem=1
        private val viewLoading=0
        var linearLayoutManager = list.layoutManager as LinearLayoutManager
        var totalItemCount : Int=0
        var lastItemVisible : Int=0
        var visibleItem : Int = 5
        var loading : Boolean = false

        init {
            list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    totalItemCount = linearLayoutManager.itemCount
                    lastItemVisible = linearLayoutManager.findLastVisibleItemPosition()

                    if (!loading && dy>0 && totalItemCount < lastItemVisible + visibleItem) {
                        if (loadMoreListener != null) {
                            loadMoreListener!!.onLoadMore()
                        }
                        loading = true
                    }
                }
            })
        }

        inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var img : ImageView = view.findViewById<View>(R.id.article_image) as ImageView
            var newsTitle : TextView = view.findViewById<View>(R.id.newsTitle) as TextView
            var description : TextView = view.findViewById<View>(R.id.description) as TextView
        }

        private inner class LoadingLayout(v: View) : RecyclerView.ViewHolder(v)
        private inner class RetryLayout(v: View) : RecyclerView.ViewHolder(v)

        override fun getItemViewType(position: Int): Int {
            return when {
                data[position].retry -> viewRetry
                data[position].title==null -> viewLoading
                else -> viewItem
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return when (viewType) {
                viewItem -> {
                    val v = LayoutInflater.from(this@NewsList)
                        .inflate(R.layout.news_list_item, parent, false)
                    MyViewHolder(v)
                }
                viewLoading -> {
                    val v = LayoutInflater.from(this@NewsList)
                        .inflate(R.layout.progress_item, parent, false)
                    LoadingLayout(v)
                }
                else -> {
                    val v = LayoutInflater.from(this@NewsList)
                        .inflate(R.layout.retry_layout, parent, false)
                    RetryLayout(v)
                }
            }
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            this.holder = holder
            if (holder is MyViewHolder){
                holder.itemView.setOnClickListener {
                    val i = Intent(holder.itemView.context,NewsDetail::class.java)
                    i.putExtra("title",data[position].title)
                    i.putExtra("url",data[position].url)
                    i.putExtra("source",data[position].source!!.name)
                    holder.itemView.context.startActivity(i)
                }

                holder.newsTitle.text= data[position].title?:""
                holder.description.text = data[position].description?:""

                if (data[position].urlToImage == null){
                    holder.img.setImageResource(R.drawable.binar_academy)
                }else{
                    Picasso.get().load(data[position].urlToImage).fit().into(holder.img)
                }

            }
            else if(holder is RetryLayout){
                holder.itemView.setOnClickListener {
                    data.removeAt(data.size-1)
                    handleLoadingList()
                    getNewsList(source!!,"5",page.toString())
                }
            }
        }

        override fun getItemCount(): Int {
            return data.size
        }

        fun setLoadMoreListener(onLoadMoreListener: OnLoadMoreListener){
            loadMoreListener = onLoadMoreListener
        }

        fun setLoading() {
            loading = false
        }
    }

}