package com.example.binaracademy

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity

class Splashscreen : AppCompatActivity() {

    private val SPLASH_DISPLAY_LENGTH = 2000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splashscreen)

        Handler().postDelayed(Runnable {
            val i = Intent(this@Splashscreen, CategoryList::class.java)
            startActivity(i)
            finish()
        }, SPLASH_DISPLAY_LENGTH.toLong())

    }

}