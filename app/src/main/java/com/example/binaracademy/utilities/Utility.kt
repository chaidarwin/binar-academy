package com.example.binaracademy.utilities

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.example.binaracademy.R
import com.example.binaracademy.io.EmptyTypeEn

object Utility {

    fun isInternetOn(activity: Context): Boolean {
        return try {
            val cm = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            cm.activeNetworkInfo!!.isConnected
        }catch (e:Exception){
            Toast.makeText(activity,activity.resources.getString(R.string.connection_disconnected),
                Toast.LENGTH_SHORT).show()
            false
        }
    }

    fun checkInternetOn(activity: Activity): Boolean {
        return try {
            val cm = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            cm.activeNetworkInfo!!.isConnected
        }catch (e:Exception){
            false
        }
    }

    fun test(img: ImageView, desc: TextView, desc2: TextView, clicklay: LinearLayout, cliktext: TextView, funasd: View.OnClickListener, action:String, context: Context){
        clicklay.setOnClickListener(funasd)
        when(action){

            EmptyTypeEn.Noconnection.result->{
                img.setImageResource(R.drawable.ic_no_conn)
                desc.text=context.getString(R.string.no_internet_connection)
                desc2.text=context.getString(R.string.please_check_your_internet_connection)
                cliktext.text=context.getString(R.string.retry)
            }

            EmptyTypeEn.Error.result->{
                img.setImageResource(R.drawable.ic_error)
                desc.text=context.getString(R.string.oops_this_page_under_construction)
                desc2.text=context.getString(R.string.please_try_again_in_few_minutes)
                cliktext.text=context.getString(R.string.retry)
            }

            EmptyTypeEn.Nopageavailable.result->{
                img.setImageResource(R.drawable.ic_error)
                desc.text=context.getString(R.string.no_page_available)
                desc2.visibility= View.GONE
                clicklay.visibility= View.GONE
            }

            EmptyTypeEn.Timeout.result->{
                img.setImageResource(R.drawable.ic_no_conn)
                desc.text=context.getString(R.string.connection_time_out)
                desc2.text=context.getString(R.string.no_internet_found)
                cliktext.text=context.getString(R.string.retry)
            }

            else ->{
                img.setImageResource(R.drawable.ic_no_data)
                desc.text=context.getString(R.string.no_data_available)
                desc2.visibility= View.GONE
                clicklay.visibility= View.GONE
            }
        }
    }


}