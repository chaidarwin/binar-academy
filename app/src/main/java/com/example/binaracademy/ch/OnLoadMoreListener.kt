package com.example.binaracademy.ch

interface OnLoadMoreListener {
    fun onLoadMore()
}