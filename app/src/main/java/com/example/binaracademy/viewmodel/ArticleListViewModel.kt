package com.example.binaracademy.viewmodel

import androidx.lifecycle.ViewModel
import com.example.binaracademy.callback.ArticleListCallback
import com.example.binaracademy.io.BaseDataRx
import com.example.binaracademy.io.RestClientNews
import com.example.binaracademy.model.ArticleData
import com.example.binaracademy.utilities.Delegate
import io.reactivex.observers.DisposableObserver

class ArticleListViewModel : ViewModel(){

    var observableArticleListData by Delegate.MutableLiveDataInit<ArticleListCallback<ArticleData>>()

    var errorRequest by Delegate.MutableLiveDataInit<Throwable>()

    fun getHeadlineNewsData(apiToken: String,baseUrl:String, sources: String,apiKey : String,pageSize:String,pages : String){
        val disposableObserver = object : DisposableObserver<ArticleListCallback<ArticleData>>(){
            override fun onComplete() {
            }

            override fun onNext(t: ArticleListCallback<ArticleData>) {
                observableArticleListData.value = t
            }

            override fun onError(e: Throwable) {
                errorRequest.value = e
            }

        }
        val apiInterface = RestClientNews.getClient(
            apiToken = apiToken,
            baseUrl = baseUrl
        )
        BaseDataRx<ArticleData>().requestArticleList(apiInterface.getHeadlineNewsData(sources,apiKey,pageSize,pages),disposableObserver)
    }

    fun getAllNewsData(apiToken: String,baseUrl:String, category: String, source : String, country:String, apiKey : String,pageSize:String,pages : String){
        val disposableObserver = object : DisposableObserver<ArticleListCallback<ArticleData>>(){
            override fun onComplete() {
            }

            override fun onNext(t: ArticleListCallback<ArticleData>) {
                observableArticleListData.value = t
            }

            override fun onError(e: Throwable) {
                errorRequest.value = e
            }

        }
        val apiInterface = RestClientNews.getClient(
            apiToken = apiToken,
            baseUrl = baseUrl
        )
        BaseDataRx<ArticleData>().requestArticleList(apiInterface.getAllNewsData(category,source,country,apiKey,pageSize,pages),disposableObserver)
    }

    fun getQueryNewsData(apiToken: String,baseUrl:String, query: String,apiKey : String,pageSize:String,pages : String){
        val disposableObserver = object : DisposableObserver<ArticleListCallback<ArticleData>>(){
            override fun onComplete() {
            }

            override fun onNext(t: ArticleListCallback<ArticleData>) {
                observableArticleListData.value = t
            }

            override fun onError(e: Throwable) {
                errorRequest.value = e
            }

        }
        val apiInterface = RestClientNews.getClient(
            apiToken = apiToken,
            baseUrl = baseUrl
        )
        BaseDataRx<ArticleData>().requestArticleList(apiInterface.getQueryNewsData(query,apiKey,pageSize,pages),disposableObserver)
    }

    fun setRetryList(data : MutableList<ArticleData>){
        data.removeAt(data.size-1)
        val a = ArticleData()
        a.retry = true
        data.add(a)
    }

    fun addLoadingList(data : MutableList<ArticleData>){
        val a = ArticleData()
        data.add(a)
    }

    fun removeLoadingList(data : MutableList<ArticleData>){
        data.removeAt(data.size - 1)
    }
}