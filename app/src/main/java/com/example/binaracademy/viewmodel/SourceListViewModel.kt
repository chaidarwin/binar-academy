package com.example.binaracademy.viewmodel

import androidx.lifecycle.ViewModel
import com.example.binaracademy.callback.SourceListCallback
import com.example.binaracademy.io.BaseDataRx
import com.example.binaracademy.io.RestClientNews
import com.example.binaracademy.model.SourceData
import com.example.binaracademy.utilities.Delegate
import io.reactivex.observers.DisposableObserver

class SourceListViewModel : ViewModel() {

    var observableSourceListData by Delegate.MutableLiveDataInit<SourceListCallback<SourceData>>()

    var errorRequest by Delegate.MutableLiveDataInit<Throwable>()

    fun getAllSourceData(apiToken: String,baseUrl:String, category: String,apiKey : String,pageSize:String,pages : String){
        val disposableObserver = object : DisposableObserver<SourceListCallback<SourceData>>(){
            override fun onComplete() {
            }

            override fun onNext(t: SourceListCallback<SourceData>) {
                observableSourceListData.value = t
            }

            override fun onError(e: Throwable) {
                errorRequest.value = e
            }

        }
        val apiInterface = RestClientNews.getClient(
            apiToken = apiToken,
            baseUrl = baseUrl
        )
        BaseDataRx<SourceData>().requestSourceList(apiInterface.getSourcesData(category,apiKey,pageSize,pages),disposableObserver)
    }

    fun setRetryList(data : MutableList<SourceData>){
        data.removeAt(data.size-1)
        val a = SourceData()
        a.retry = true
        data.add(a)
    }

    fun addLoadingList(data : MutableList<SourceData>){
        val a = SourceData()
        data.add(a)
    }

    fun removeLoadingList(data : MutableList<SourceData>){
        data.removeAt(data.size - 1)
    }
}